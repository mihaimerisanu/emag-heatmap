Emag Heatmap App
==================================

# Installation #

Install dependencies:

* docker. See [https://docs.docker.com/engine/installation](https://docs.docker.com/engine/installation)
* docker-compose. See [docs.docker.com/compose/install](https://docs.docker.com/compose/install/)

Clone repository `git clone https://mihaimerisanu@bitbucket.org/mihaimerisanu/emag-heatmap.git emg`
`cd` to `emg` directory and run `docker-compose up -d`.

Open the php-fpm container bash with `docker-compose exec php-fpm bash`
Run `composer install`
Run `php bin/console doctrine:migrations:migrate` to set up database

# Usage #

After installation, open browser and go to http://localhost:48000

1. To store data, use `POST /api/hits`
    ``` 
   {
    "link": "string", #Visited uri ("/cars/audi")
    "linkType": "string", #Type of the uri ("product")
    "timestamp": "2022-03-16T22:36:04.235Z",
    "customerId": "string" #Customer identifier ("customer_1")
    } 
    ```

2. Reports

    a. How many hits did a link receive in a provided time interval?
       Use `GET /api/link_hit_reports` with parameters: link, fromTimestamp, toTimestamp.
       The "count" property in the response will provide the number of hits
   
    b. How many hits did each page type receive in a provided time interval?
       Use `GET /api/type_hit_reports` with parameters: linkType, fromTimestamp, toTimestamp.
       The "count" property in the response will provide the number of hits


3. Customer Journey

    Use `GET /api/journeys` with parameter customerId. This will provide a Hit list in chronological order and a Customer id list containing the customers with the same journey.
    A journey id is also provided. This can be used with `GET /api/journey/{id}` to retrieve the same journey information.

# About #
Web stack: Nginx, PHP 8.1, PostgreSQL, Elasticsearch

Framework: Symfony 6 with Doctrine, FOSElastica and ApiPlatform
