<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Entity\LinkHitReport;
use App\Entity\TypeHitReport;
use App\Filter\FromTimestampFilter;
use App\Filter\LinkFilter;
use App\Filter\ToTimestampFilter;
use App\Filter\TypeFilter;
use Elastica\Query\Range;
use Elastica\Query\Term;
use Elastica\Query\Terms;
use FOS\ElasticaBundle\Finder\FinderInterface;

class HitReportDataProvider implements ItemDataProviderInterface, ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var FinderInterface
     */
    protected $finder;

    public function __construct(FinderInterface $hitFinder)
    {
        $this->finder = $hitFinder;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        $idArr = explode('-', $id);
        $boolQuery = new \Elastica\Query\BoolQuery();

        /** @var \DateTimeImmutable $fromTimestamp */
        $fromTimestamp = (new \DateTimeImmutable())->setTimestamp($idArr[0]);
        if($fromTimestamp) {
            $boolQuery->addFilter(new Range('timestamp', [
                'gt' => $fromTimestamp->format('c'),
            ]));
        }

        /** @var \DateTimeImmutable $toTimestamp */
        $toTimestamp = (new \DateTimeImmutable())->setTimestamp($idArr[1]);
        if($toTimestamp) {
            $boolQuery->addFilter(new Range('timestamp', [
                'lt' => $toTimestamp->format('c'),
            ]));
        }

        if(LinkHitReport::class === $resourceClass) {
            $link = $idArr[2] ?? null;
            if ($link) {
                $boolQuery->addFilter(new Term(['linkHash' => $link]));
            }

            $resource = new LinkHitReport();
            $resource->setLink($link);
        }
        elseif(TypeHitReport::class === $resourceClass) {
            $type = $idArr[2] ?? null;
            if ($type) {
                $boolQuery->addFilter(new Term(['linkType' => $type]));
            }

            $resource = new TypeHitReport();
            $resource->setType($type);
        }
        else {
            return null;
        }

        $data = $this->finder->createPaginatorAdapter($boolQuery);
        $resource->setCount($data->getTotalHits())
            ->setFromTimestamp($fromTimestamp)
            ->setToTimestamp($toTimestamp);

        return $resource;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = [])
    {
        $boolQuery = new \Elastica\Query\BoolQuery();

        /** @var \DateTimeImmutable $fromTimestamp */
        $fromTimestamp = $context[FromTimestampFilter::KEY] ?? null;
        if($fromTimestamp) {
            $boolQuery->addFilter(new Range('timestamp', [
                'gt' => $fromTimestamp->format('c'),
            ]));
        }

        /** @var \DateTimeImmutable $toTimestamp */
        $toTimestamp = $context[ToTimestampFilter::KEY] ?? null;
        if($toTimestamp) {
            $boolQuery->addFilter(new Range('timestamp', [
                'lt' => $toTimestamp->format('c'),
            ]));
        }

        if(LinkHitReport::class === $resourceClass) {
            $link = $context[LinkFilter::KEY] ?? null;
            if ($link) {
                $boolQuery->addFilter(new Term(['linkHash' => md5($link)]));
            }

            $resource = new LinkHitReport();
            $resource->setLink($link);
        }
        elseif(TypeHitReport::class === $resourceClass) {
            $type = $context[TypeFilter::KEY] ?? null;
            if ($type) {
                $boolQuery->addFilter(new Term(['linkType' => $type]));
            }

            $resource = new TypeHitReport();
            $resource->setType($type);
        }
        else {
            return [];
        }

        $data = $this->finder->createPaginatorAdapter($boolQuery);
        $resource->setCount($data->getTotalHits())
            ->setFromTimestamp($fromTimestamp)
            ->setToTimestamp($toTimestamp);

        return [$resource];
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return LinkHitReport::class === $resourceClass || TypeHitReport::class === $resourceClass;
    }

}