<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Hit;
use App\Entity\Journey;
use App\Filter\CustomerIdFilter;
use Elastica\Aggregation\Terms;
use Elastica\Query;
use Elastica\Query\Range;
use Elastica\Query\Term;
use FOS\ElasticaBundle\Finder\FinderInterface;

class JourneyDataProvider implements ItemDataProviderInterface, ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var FinderInterface
     */
    protected $finder;

    public function __construct(FinderInterface $hitFinder)
    {
        $this->finder = $hitFinder;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        $resource = new Journey();

        // Get hits for customer
        $boolQuery = new Query\BoolQuery();
        $customerId = $context[CustomerIdFilter::KEY] ?? null;
        if ($customerId) {
            $boolQuery->addFilter(new Term(['customerId' => $customerId]));
        }

        $query = new Query();
        $query
            ->addSort(array('timestamp' => array('order' => 'asc')))
            ->setQuery($boolQuery);

        /** @var Hit[] $hits */
        $hits = $this->finder->find($query);
        if(empty($hits)) {
            return [];
        }

        $resource
            ->setHits($hits)
            ->setId(!empty($hits) ? end($hits)->getJourneyId() : null);

        // Get hits with same journey
        if($resource->getId()) {
            $this->populateHitsByJourneyId($resource);
        }

        return [$resource];
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?object
    {
        $resource = new Journey();
        $resource->setId($id);

        // Get reference hit
        $boolQuery = new Query\BoolQuery();
        $boolQuery->addFilter(new Term(['journeyId' => $id]));

        $query = new Query();
        $query->setSize(1)
            ->setQuery($boolQuery);
        $refHit = $this->finder->find($query);
        if(empty($refHit)) {
            return null;
        }

        /** @var Hit $refHit */
        $refHit = $refHit[0];

        // Get hit list for journey
        $boolQuery = new Query\BoolQuery();
        $boolQuery->addFilter(new Term(['customerId' => $refHit->getCustomerId()]));
        $boolQuery->addFilter(new Range('timestamp', [
            'lte' => $refHit->getTimestamp()->format('c'),
        ]));

        $query = new Query();
        $query
            ->addSort(array('timestamp' => array('order' => 'asc')))
            ->setQuery($boolQuery);

        $hits = $this->finder->find($query);
        $resource->setHits($hits);

        $this->populateHitsByJourneyId($resource);

        return $resource;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Journey::class === $resourceClass;
    }

    protected function populateHitsByJourneyId(Journey $resource)
    {
        $query = new Query();
        $boolQuery = new Query\BoolQuery();
        $boolQuery->addFilter(new Term(['journeyId' => $resource->getId()]));
        $query->setQuery($boolQuery);

        /** @var Hit[] $hits */
        $hits = $this->finder->find($query);
        foreach ($hits as $hit) {
            $resource->addCustomerId($hit->getCustomerId());
        }
    }
}