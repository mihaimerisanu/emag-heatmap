<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;

abstract class AbstractReport
{
    /** @var \DateTimeImmutable */
    protected $fromTimestamp;

    /** @var \DateTimeImmutable */
    protected $toTimestamp;

    /** @var int */
    protected $count;

    /**
     * @return \DateTimeImmutable
     */
    public function getFromTimestamp(): \DateTimeImmutable
    {
        return $this->fromTimestamp;
    }

    /**
     * @param \DateTimeImmutable $fromTimestamp
     * @return LinkHitReport
     */
    public function setFromTimestamp(\DateTimeImmutable $fromTimestamp): self
    {
        $this->fromTimestamp = $fromTimestamp;
        return $this;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getToTimestamp(): \DateTimeImmutable
    {
        return $this->toTimestamp;
    }

    /**
     * @param \DateTimeImmutable $toTimestamp
     * @return LinkHitReport
     */
    public function setToTimestamp(\DateTimeImmutable $toTimestamp): self
    {
        $this->toTimestamp = $toTimestamp;
        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return LinkHitReport
     */
    public function setCount(int $count): self
    {
        $this->count = $count;
        return $this;
    }
}