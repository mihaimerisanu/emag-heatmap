<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\HitRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity(repositoryClass: HitRepository::class)]
#[ApiResource(
    itemOperations: ["get", "put", "delete"],
    collectionOperations: ["get", "post"],
    denormalizationContext: ['groups' => ['write']],
    normalizationContext: ['groups' => ['read']],
    paginationEnabled: true,
)]
class Hit
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(["read", "elastic"])]
    private $id;

    #[ORM\Column(type: 'text')]
    #[Groups(["read", "write", "elastic", "journey"])]
    private $link;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["read", "write", "elastic"])]
    private $linkType;

    #[ORM\Column(type: 'datetime')]
    #[Groups(["read", "write", "elastic", "journey"])]
    private $timestamp;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["read", "write", "elastic"])]
    private $customerId;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["elastic"])]
    private $linkHash;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["read", "elastic"])]
    private $journeyId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;
        $this->setLinkHash(md5($link));

        return $this;
    }

    public function getLinkType(): ?string
    {
        return $this->linkType;
    }

    public function setLinkType(string $linkType): self
    {
        $this->linkType = $linkType;

        return $this;
    }

    public function getTimestamp(): ?\DateTimeInterface
    {
        return $this->timestamp;
    }

    public function setTimestamp(\DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    public function setCustomerId(string $customerId): self
    {
        $this->customerId = $customerId;

        return $this;
    }

    public function getLinkHash(): ?string
    {
        return $this->linkHash;
    }

    public function setLinkHash(string $linkHash): self
    {
        $this->linkHash = $linkHash;

        return $this;
    }

    public function getJourneyId(): ?string
    {
        return $this->journeyId;
    }

    public function setJourneyId(string $journeyId): self
    {
        $this->journeyId = $journeyId;

        return $this;
    }
}
