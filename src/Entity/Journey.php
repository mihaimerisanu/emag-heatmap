<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Filter\CustomerIdFilter;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    itemOperations: ["get"],
    collectionOperations: ["get"],
    paginationEnabled: false,
    normalizationContext: ["groups"=>["journey"]]
)]
#[ApiFilter(CustomerIdFilter::class)]
class Journey
{
    #[ApiProperty(identifier: true)]
    #[Groups(["journey"])]
    protected $id;

    /** @var [] */
    #[Groups(["journey"])]
    protected $customerIds = [];

    /** @var Hit[] */
    #[Groups(["journey"])]
    protected $hits;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Journey
     */
    public function setId($id): Journey
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerIds()
    {
        return $this->customerIds;
    }

    /**
     * @param mixed $customerIds
     * @return Journey
     */
    public function setCustomerIds($customerIds): Journey
    {
        $this->customerIds = $customerIds;
        return $this;
    }

    /**
     * @param string $customerId
     * @return $this
     */
    public function addCustomerId($customerId): Journey
    {
        $this->customerIds[] = $customerId;
        return $this;
    }

    /**
     * @return Hit[]
     */
    public function getHits(): array
    {
        return $this->hits;
    }

    /**
     * @param Hit[] $hits
     * @return Journey
     */
    public function setHits(array $hits): Journey
    {
        $this->hits = $hits;
        return $this;
    }


}