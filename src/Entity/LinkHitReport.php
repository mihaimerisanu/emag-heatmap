<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Filter\FromTimestampFilter;
use App\Filter\LinkFilter;
use App\Filter\ToTimestampFilter;

#[ApiResource(itemOperations: ["get"], collectionOperations: ["get"], paginationEnabled: false)]
#[ApiFilter(LinkFilter::class)]
#[ApiFilter(FromTimestampFilter::class)]
#[ApiFilter(ToTimestampFilter::class)]
class LinkHitReport extends AbstractReport
{
    #[ApiProperty()]
    protected $link;

    #[ApiProperty(identifier: true)]
    public function getId()
    {
        return $this->fromTimestamp->getTimestamp().'-'.$this->toTimestamp->getTimestamp().'-'.md5($this->link);
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return LinkHitReport
     */
    public function setLink(string $link): self
    {
        $this->link = $link;
        return $this;
    }
}