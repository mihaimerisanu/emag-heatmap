<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Filter\FromTimestampFilter;
use App\Filter\TypeFilter;
use App\Filter\ToTimestampFilter;

#[ApiResource(itemOperations: ["get"], collectionOperations: ["get"], paginationEnabled: false)]
#[ApiFilter(TypeFilter::class)]
#[ApiFilter(FromTimestampFilter::class)]
#[ApiFilter(ToTimestampFilter::class)]
class TypeHitReport extends AbstractReport
{
    #[ApiProperty()]
    protected $type;

    #[ApiProperty(identifier: true)]
    public function getId()
    {
        return $this->fromTimestamp->getTimestamp().'-'.$this->toTimestamp->getTimestamp().'-'.$this->type;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return TypeHitReport
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }
}