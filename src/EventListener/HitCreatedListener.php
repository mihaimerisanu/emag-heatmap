<?php

namespace App\EventListener;

use App\Entity\Hit;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class HitCreatedListener
{
    /**
     * @param Hit $hit
     * @param LifecycleEventArgs $event
     * @return void
     */
    public function prePersist(Hit $hit, LifecycleEventArgs $event): void
    {
        $em = $event->getObjectManager();
        /** @var Hit $lastHit */
        $lastHit = $em->getRepository('App:Hit')
            ->findOneBy(['customerId' => $hit->getCustomerId()], ['timestamp' => 'DESC']);

        $hit->setJourneyId(md5($lastHit ? $lastHit->getJourneyId().$hit->getLinkHash() : $hit->getLinkHash()));
    }
}