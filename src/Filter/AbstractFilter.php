<?php

namespace App\Filter;

use Symfony\Component\PropertyInfo\Type;

class AbstractFilter
{
    public const KEY = null;

    /**
     * @param string $resourceClass
     * @return array[]
     */
    public function getDescription(string $resourceClass): array
    {
        return [
            $this::KEY => [
                'property' => null,
                'type' => Type::BUILTIN_TYPE_STRING,
                'required' => true,
            ]
        ];
    }
}