<?php

namespace App\Filter;

use Symfony\Component\HttpFoundation\Request;

class AbstractStringFilter extends AbstractFilter
{
    /**
     * @param Request $request
     * @param bool $normalization
     * @param array $attributes
     * @param array $context
     * @return void
     * @throws \Exception
     */
    public function apply(Request $request, bool $normalization, array $attributes, array &$context)
    {
        if(!$this::KEY) {
            throw new \Exception('KEY constant is not defined in child object');
        }

        $val = $request->query->get($this::KEY);
        if (!$val) {
            return;
        }

        $context[$this::KEY] = $val;
    }
}