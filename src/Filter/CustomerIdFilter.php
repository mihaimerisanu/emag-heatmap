<?php

namespace App\Filter;

use ApiPlatform\Core\Serializer\Filter\FilterInterface;

class CustomerIdFilter extends AbstractStringFilter implements FilterInterface
{
    public const KEY = 'customerId';
}