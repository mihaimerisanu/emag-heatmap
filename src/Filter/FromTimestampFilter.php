<?php

namespace App\Filter;

use ApiPlatform\Core\Serializer\Filter\FilterInterface;
use Symfony\Component\HttpFoundation\Request;

class FromTimestampFilter extends AbstractFilter implements FilterInterface
{
    public const KEY = 'fromTimestamp';

    public function apply(Request $request, bool $normalization, array $attributes, array &$context)
    {
        $ts = $request->query->get(self::KEY);
        if (!$ts) {
            return;
        }

        try {
            $tsd = new \DateTimeImmutable($ts);
        } catch (\Exception $e) {
            return;
        }

        $context[self::KEY] = $tsd;
    }
}