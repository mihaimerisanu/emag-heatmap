<?php

namespace App\Filter;

use ApiPlatform\Core\Serializer\Filter\FilterInterface;

class LinkFilter extends AbstractStringFilter implements FilterInterface
{
    public const KEY = 'link';
}