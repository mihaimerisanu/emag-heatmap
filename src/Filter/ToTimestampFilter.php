<?php

namespace App\Filter;

use ApiPlatform\Core\Serializer\Filter\FilterInterface;
use Symfony\Component\HttpFoundation\Request;

class ToTimestampFilter extends AbstractFilter implements FilterInterface
{
    public const KEY = 'toTimestamp';

    public function apply(Request $request, bool $normalization, array $attributes, array &$context)
    {
        $ts = $request->query->get(self::KEY);
        if (!$ts) {
            return;
        }

        try {
            $tsd = new \DateTimeImmutable($ts);
            $context[self::KEY] = $tsd;
        } catch (\Exception $e) {
            return;
        }
    }
}