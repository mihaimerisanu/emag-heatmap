<?php

namespace App\Filter;

use ApiPlatform\Core\Serializer\Filter\FilterInterface;

class TypeFilter extends AbstractStringFilter implements FilterInterface
{
    public const KEY = 'linkType';
}