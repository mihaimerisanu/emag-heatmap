<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

class HitTest extends ApiTestCase
{
    public function testSomething(): void
    {
        static::createClient()->request('GET', '/api/hits');

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(['@id' => '/api/hits']);
    }
}
