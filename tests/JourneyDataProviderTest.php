<?php

namespace App\Tests;

use App\DataProvider\JourneyDataProvider;
use App\Entity\Hit;
use App\Entity\Journey;
use App\Filter\CustomerIdFilter;
use FOS\ElasticaBundle\Finder\FinderInterface;
use PHPUnit\Framework\Constraint\Callback;
use PHPUnit\Framework\TestCase;

class JourneyDataProviderTest extends TestCase
{
    public function testGetCollection(): void
    {
        $hits = [];
        for($i=0; $i<10; $i++) {
            $hit = new Hit();
            $hit->setTimestamp(new \DateTimeImmutable())
                ->setLink('/link/'.$i)
                ->setJourneyId(md5('a'.$i))
                ->setCustomerId('c_'.$i)
                ->setLinkType('product');
            $hits[] = $hit;
        }
        $hitFinder = $this->createMock(FinderInterface::class);
        $hitFinder->expects($this->any())
            ->method('find')
            ->willReturn($hits);

        $journeyDataProvider = new JourneyDataProvider($hitFinder);

        $this->assertIsArray($journeyDataProvider->getCollection(Journey::class, null, [CustomerIdFilter::KEY => 'c_4']));
        $this->assertCount(1, $journeyDataProvider->getCollection(Journey::class, null, [CustomerIdFilter::KEY => 'c_4']));
        $this->assertThat($journeyDataProvider->getCollection(Journey::class, null, [CustomerIdFilter::KEY => 'c_4']), new Callback(function($r) use ($hits) {
            $j = $r[0];
            return $j instanceof Journey && $j->getHits() == $hits && count($j->getCustomerIds()) == 10;
        }));

        $hitFinder = $this->createMock(FinderInterface::class);
        $hitFinder->expects($this->any())
            ->method('find')
            ->willReturn([]);

        $journeyDataProvider = new JourneyDataProvider($hitFinder);

        $this->assertCount(0, $journeyDataProvider->getCollection(Journey::class, null, [CustomerIdFilter::KEY => 'c_4']));
    }
}
